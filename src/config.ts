import * as Phaser from 'phaser';

const width = 1777;
const height = 800;

export default {
  type: Phaser.AUTO,
	width,
    height,
	physics: {
		default: 'arcade',
		arcade: {
            debug: import.meta.env.DEV ? true : false,
		}
	},
    parent: 'game',
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width,
        height,
    },
};
