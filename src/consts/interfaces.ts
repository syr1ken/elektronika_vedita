import ScreenOverlay from "../game/ScreenOverlay";
import Difficulty from "../game/Difficulty";
import Score from "../game/Score";

export interface Position {
    x: number;
    y: number;
}

export interface TextStyle {
    text_style: Phaser.Types.GameObjects.Text.TextStyle;
    color: string;
    hoverColor: string;
    activeColor: string;
}

export interface VolkObject {
    position: Position;
    health: HealthObject;
    frames: number;
}

export interface EggObject {
    position: Position;
    position_string: string;
    broken_egg_position_string: string;
    frames: number;
    id: string;
}

export interface ScoreObject {
    position: Position;
    score: number;
    style: Phaser.Types.GameObjects.Text.TextStyle;
}

export interface DifficultyObject {
    min: number;
    max: number;
    difficulty: number;
}

export interface HealthObject {
    position: Position;
    current_health: number;
    max_health: number;
    frames: number;
}

export interface GameOverTextObject {
    position: Position;
    text: string;
    style: TextStyle;
}

export interface GameOverObject {
    position: Position;
    score_text: GameOverTextObject;
    start_new_game_btn: GameOverTextObject;
    go_to_main_menu_btn: GameOverTextObject;
}

export interface GameOverSceneObject {
    score: Score;
}

export interface GameMenuTextObject {
    position: Position;
    text: string;
    style: TextStyle;
}

export interface GameMenuObject {
    position: Position;
    screen_overlay: ScreenOverlay;
    resume_game_btn: GameMenuTextObject;
    difficulty_text: GameMenuTextObject;
    go_to_main_menu_btn: GameMenuTextObject;
    start_new_game_btn: GameMenuTextObject;
}

export interface GamePauseMenuSceneObject {
    difficulty: Difficulty;
}

export interface StartGameMenuTextObject {
    position: Position;
    text: string;
    style: TextStyle;
}

export interface StartGameMenuObject {
    position: Position;
    start_new_game_btn: StartGameMenuTextObject;
    version_text: StartGameMenuTextObject;
}

export interface FullScreenBtnSpriteObject {
    position: Position;
    full_screen_sprite: {
        position: Position;
        text: string;
        style: Phaser.Types.GameObjects.Text.TextStyle;
    };
}

export interface FullScreenBtnObject {
    position: Position;
}

export interface VolumeBtnObject {
    position: Position;
}

export interface ScreenOverlayObject {
    position: Position;
}