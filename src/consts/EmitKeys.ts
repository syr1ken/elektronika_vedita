enum EmitKeys {
    top_left_button_clicked = "top_left_button_clicked",
    bottom_left_button_clicked = "bottom_left_button_clicked",
    top_right_button_clicked = "top_right_button_clicked",
    bottom_right_button_clicked = "bottom_right_button_clicked",
    menu_button_clicked = "menu_button_clicked",
}

export default EmitKeys;
