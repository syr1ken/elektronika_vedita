import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import ScreenOverlay from "../game/ScreenOverlay";
import GameMenu from "../game/GameMenu";
import Difficulty from "../game/Difficulty";
import { GamePauseMenuSceneObject } from "../consts/interfaces";

export default class GamePauseScene extends Phaser.Scene {
    private screen_overlay!: ScreenOverlay;
    private game_menu!: GameMenu;
    private difficulty!: Difficulty;

    constructor() {
        super(SceneKeys.GamePauseScene);
    }

    init(data: GamePauseMenuSceneObject) {
        this.difficulty = data.difficulty;
    }

    create() {
        this.screen_overlay = new ScreenOverlay(this, {
            position: {
                x: 330,
                y: 3,
            },
        });

        this.screen_overlay.setVisibility(true);

        const menu_overlay_text_style = {
            fontFamily: "interbold",
            fontSize: "58px",
            color: "#DDD",
            align: "center",
        };

        const difficulty_text_style = {
            fontFamily: "interbold",
            fontSize: "32px",
            color: "#DDD",
            align: "center",
        };

        this.game_menu = new GameMenu(this, {
            position: {
                x: 371,
                y: 41,
            },
            screen_overlay: this.screen_overlay,
            resume_game_btn: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 - 150,
                },
                text: "ПРОДОЛЖИТЬ",
                style: {
                    text_style: menu_overlay_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
            start_new_game_btn: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 - 50,
                },
                text: "НАЧАТЬ НОВУЮ ИГРУ",
                style: {
                    text_style: menu_overlay_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
            go_to_main_menu_btn: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 + 50,
                },
                text: `ВЕРНУТЬСЯ В ГЛАВНОЕ МЕНЮ`,
                style: {
                    text_style: menu_overlay_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
            difficulty_text: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 + 150,
                },
                text: `СЛОЖНОСТЬ: ${this.difficulty.getDifficulty}`,
                style: {
                    text_style: difficulty_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
        });

        this.game_menu.setVisibility(true);
    }

    update(t: number, dt: number): void {}
}
