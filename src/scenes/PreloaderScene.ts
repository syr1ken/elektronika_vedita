import * as Phaser from 'phaser';

import TextureKeys from "../consts/TextureKeys";
import SceneKeys from "../consts/SceneKeys";
import AudioKeys from "../consts/AudioKeys";

export default class PreloaderScene extends Phaser.Scene {
    constructor() {
        super(SceneKeys.PreloaderScene);
    }

    preload() {
        this.load.image(TextureKeys.screen_overlay, "/assets/textures/x_800_envelop_screen_overlay.svg");
        
        this.load.spritesheet(TextureKeys.volk, "/assets/textures/x_800_envelop_volk_sprite.svg", {
            frameWidth: 575,
            frameHeight: 356,
        });

        this.load.spritesheet(TextureKeys.health, "/assets/textures/x_800_envelop_volk_health_sprite.svg", {
            frameWidth: 244,
            frameHeight: 60,
        });

        this.load.spritesheet(TextureKeys.egg_top_left, "/assets/textures/x_800_envelop_egg_top_left_sprite.svg", {
            frameWidth: 270,
            frameHeight: 115,
        });

        this.load.spritesheet(TextureKeys.egg_bottom_left, "/assets/textures/x_800_envelop_egg_bottom_left_sprite.svg", {
            frameWidth: 266,
            frameHeight: 110,
        });

        this.load.spritesheet(TextureKeys.egg_top_right, "/assets/textures/x_800_envelop_egg_top_right_sprite.svg", {
            frameWidth: 266,
            frameHeight: 110,
        });

        this.load.spritesheet(TextureKeys.egg_bottom_right, "/assets/textures/x_800_envelop_egg_bottom_right_sprite.svg", {
            frameWidth: 265,
            frameHeight: 113,
        });

        this.load.image(TextureKeys.broken_egg, "/assets/textures/x_800_envelop_broken_egg.svg");

        this.load.audio(AudioKeys.egg_tick, ["/assets/sound/egg_tick.wav"]);
        this.load.audio(AudioKeys.egg_broke, ["/assets/sound/egg_broke.wav"]);
        this.load.audio(AudioKeys.egg_catch, ["/assets/sound/egg_catch.wav"]);
        this.load.audio(AudioKeys.game_over, ["/assets/sound/game_over.wav"]);
    }

    create() {
        this.scene.start(SceneKeys.StartGameMenuScene);
        this.scene.bringToTop(SceneKeys.BackgroundScene);
        this.scene.bringToTop(SceneKeys.UIScene);
        this.scene.bringToTop(SceneKeys.StartGameMenuScene);
    }
}
