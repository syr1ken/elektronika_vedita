import * as Phaser from 'phaser';

import { EggObject } from "../consts/interfaces";

const score_points: number = 2;

export default class Egg extends Phaser.GameObjects.Container {
    private egg_sprite!: Phaser.GameObjects.Sprite;
    public egg_position!: string;
    public broken_egg_position!: string;
    public frames!: number;
    public current_frame!: number;

    constructor(scene: Phaser.Scene, egg: EggObject) {
        super(scene, egg.position.x, egg.position.y);
        this.egg_sprite = scene.add.sprite(egg.position.x, egg.position.y, egg.id);
        this.egg_position = egg.position_string;
        this.broken_egg_position = egg.broken_egg_position_string;
        this.frames = egg.frames;
        this.current_frame = 0;
    }

    public static get score_points(): number {
        return score_points;
    }

    public setNextFrame(): void {
        this.current_frame++;
        this.egg_sprite.setFrame(this.current_frame);
    }

    public destroy(): void {
        this.egg_sprite.destroy();
    }
}
