import * as Phaser from 'phaser';

import TextureKeys from "../consts/TextureKeys";
import { HealthObject } from "../consts/interfaces";

export default class Health extends Phaser.GameObjects.Container {
    public health_sprite!: Phaser.GameObjects.Sprite;
    public current_health!: number;
    public max_health!: number;
    public current_frame: number = 0;
    public frames!: number;

    constructor(scene: Phaser.Scene, health: HealthObject) {
        super(scene, health.position.x, health.position.y);
        this.health_sprite = scene.add.sprite(health.position.x, health.position.y, TextureKeys.health);
        this.current_health = health.current_health;
        this.max_health = health.max_health;
        this.frames = health.frames;
    }

    public destroy(): void {
        this.health_sprite.destroy();
    }
}
