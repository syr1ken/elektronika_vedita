import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import { StartGameMenuObject } from "../consts/interfaces";

export default class StartGameMenu extends Phaser.GameObjects.Container {
    private start_new_game_btn!: Phaser.GameObjects.Text;
    private start_new_game_container!: Phaser.GameObjects.Container;
    private version_text!: Phaser.GameObjects.Text;

    constructor(scene: Phaser.Scene, start_game_menu_overlay: StartGameMenuObject) {
        super(scene, start_game_menu_overlay.position.x, start_game_menu_overlay.position.y);

        this.start_new_game_btn = scene.add
            .text(
                start_game_menu_overlay.start_new_game_btn.position.x,
                start_game_menu_overlay.start_new_game_btn.position.y,
                start_game_menu_overlay.start_new_game_btn.text,
                start_game_menu_overlay.start_new_game_btn.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.start_new_game_container = scene.add
            .container(
                start_game_menu_overlay.start_new_game_btn.position.x,
                start_game_menu_overlay.start_new_game_btn.position.y
            )
            .setSize(this.start_new_game_btn.displayWidth, this.start_new_game_btn.displayHeight)
            .setInteractive();

        this.setUpTextButtonsEvents(this.start_new_game_container, this.start_new_game_btn, {
            color: start_game_menu_overlay.start_new_game_btn.style.color,
            hoverColor: start_game_menu_overlay.start_new_game_btn.style.hoverColor,
            activeColor: start_game_menu_overlay.start_new_game_btn.style.activeColor,
        });

        this.start_new_game_container.on("pointerup", function (pointer: any) {
            scene.scene.stop(SceneKeys.StartGameMenuScene);
            scene.scene.start(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.BackgroundScene);
            scene.scene.bringToTop(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.UIScene);
        });

        this.version_text = scene.add
            .text(
                start_game_menu_overlay.version_text.position.x,
                start_game_menu_overlay.version_text.position.y,
                start_game_menu_overlay.version_text.text,
                start_game_menu_overlay.version_text.style.text_style
            )
            .setOrigin(0.5, 0.5);
    }

    private setUpTextButtonsEvents(
        container: Phaser.GameObjects.Container,
        text: Phaser.GameObjects.Text,
        style: any,
    ): void {
        container.on("pointerdown", function (pointer: any) {
            text.setColor(style.activeColor);
        });
        container.on("pointerout", function (pointer: any) {
            text.setColor(style.color);
        });
        container.on("pointerover", function (pointer: any) {
            text.setColor(style.hoverColor);
        });
    }

    public setVisibility(visible: boolean): void {
        this.start_new_game_btn.visible = visible;
        this.version_text.visible = visible;
        this.start_new_game_container.visible = visible;
    }

    public destroy(): void {
        this.start_new_game_container.destroy();
        this.start_new_game_btn.destroy();
        this.version_text.destroy();
    }
}
