import * as Phaser from 'phaser';

import { DifficultyObject } from "../consts/interfaces";

export default class Difficulty extends Phaser.GameObjects.Container {
    private difficulty: number = 1;
    private min: number = 1;
    private max: number = 2;

    constructor(scene: Phaser.Scene, difficulty: DifficultyObject) {
        super(scene);
        this.difficulty = difficulty.difficulty;
        this.min = difficulty.min;
        this.max = difficulty.max;
    }

    public get getDifficulty(): number {
        return this.difficulty;
    }

    public set setDifficulty(value: number) {
        if (value > 0) {
            this.difficulty = value;
        }
    }

    public get getMaxDifficulty(): number {
        return this.max;
    }

    public get getMinDifficulty(): number {
        return this.min;
    }
}
